//
//  GroupManager.swift
//  VKTest
//
//  Created by Michael Nikolaev on 25.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import UIKit

class GroupManager {
    
    var imageManager = ImageManager()
    let userManager = UserManager()
    
    func getGroupMessage(uid: Int, title: String, accessToken: String, completion: @escaping (_ groupMesage: Chat)->Void) {
        Thread.sleep(forTimeInterval: TimeInterval(0.3)) // For "Too many requests per second" issue
        
        let historyRequest = VKRequest(string: "https://api.vk.com/method/messages.getChat?access_token=\(accessToken)&chat_id=\(uid)")
        guard let urlRequest = historyRequest.urlRequest else { return }
        
        URLSession.shared.dataTask(with: urlRequest, completionHandler: {
            data, response, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            guard let serializedData = try! JSONSerialization.jsonObject(with: data!, options: [.allowFragments]) as? [String:Any] else { return }
            
            if let object = serializedData["response"] as? [String:Any] {
                
                var isMuted = false
                if let pushSettings = object["push_settings"] as? [String: Any] {
                    if let disabledUntil = pushSettings["disabled_until"] as? Int {
                        if disabledUntil != 0 {
                            isMuted = true
                        }
                    }
                }
                
                if let photo = object["photo_100"] as? String {
                    self.imageManager.downloadImage(url: URL(string: photo)!, completion: {
                        image in
                        let chat = Chat(uid: uid, fullName: title, photo: image, verified: false, muted: isMuted, previewPhotos: nil)
                        completion(chat)
                    })
                } else {
                    if let users = object["users"] as? Array<Int> {
                        if users.count == 0 {
                            completion(Chat(uid: uid, fullName: title, photo: nil, verified: false, muted: isMuted, previewPhotos: nil))
                            return
                        }
                        
                        var userIds = [Int]()
                        var userImages = [UIImage]()
                        for (i, user) in users.enumerated() {
                            userIds.append(user)
                            if i > 2 {
                                break
                            }
                        }
                        
                        for id in userIds {
                            self.userManager.getUser(uid: id, completion: {
                                user in
                                userImages.append(user.photo!)
                                
                                if userImages.count == userIds.count {
                                    completion(Chat(uid: uid, fullName: title, photo: nil, verified: false, muted: isMuted, previewPhotos: userImages))
                                }
                            })
                        }
                    }
                }
            }
        }).resume()
    }
}
