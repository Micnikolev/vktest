//
//  CommunityManager.swift
//  VKTest
//
//  Created by Michael Nikolaev on 25.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import UIKit

class CommunityManager {
    func getCommunity(uid: Int, accessToken: String, completion: @escaping(_ community: Community)->Void) {
        let imageManager = ImageManager()
        
        var uid = uid
        if uid < 0 {
            uid = uid * -1
        }
        
        let historyRequest = VKRequest(string: "https://api.vk.com/method/groups.getById?access_token=\(accessToken)&group_ids=\(uid)&fields=verified")
        guard let urlRequest = historyRequest.urlRequest else { return }
        
        URLSession.shared.dataTask(with: urlRequest, completionHandler: {
            data, response, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let serializedData = try! JSONSerialization.jsonObject(with: data!, options: [.allowFragments]) as? [String:Any] else { return }
            
            guard let responseArray = serializedData["response"] as? Array<AnyObject> else { return }
            
            if let message = responseArray[0] as? [String: Any] {
                guard let id = message["gid"] as? Int,
                    let name = message["name"] as? String,
                    let photo = message["photo_medium"] as? String else {
                        return
                }
                let verified = message["verified"] as? Int ?? 0
                
                var isVerified = false
                if verified == 1 {
                    isVerified = true
                }
                
                imageManager.downloadImage(url: URL(string: photo)!, completion: {
                    image in
                    let community = Community(uid: id, fullName: name, photo: image, verified: isVerified)
                    completion(community)
                })
            }
        }).resume()
    }
}
