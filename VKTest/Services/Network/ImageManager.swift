//
//  ImageService.swift
//  VKTest
//
//  Created by Michael Nikolaev on 23.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import UIKit

class ImageManager {
    func downloadImage(url: URL, completion: @escaping (_ image: UIImage) -> Void) {
        getDataFromUrl(url: url) { data, response, error in
            guard let data = data, error == nil, let image = UIImage(data: data) else { return }
            completion(image)
        }
    }
    
    private func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
}
