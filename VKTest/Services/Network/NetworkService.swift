//
//  NetworkService.swift
//  VKTest
//
//  Created by Michael Nikolaev on 23.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import UIKit

class NetworkService {
    
    let imageManager = ImageManager()
    
    func getMessages(accessToken: String, offset: Int, completion: @escaping (_ messages: [IMessage]) -> Void) {
        let messagesManager = MessagesManager()
        messagesManager.getMessagesHistory(accessToken: accessToken, offset: offset, completion: completion)
    }
}
