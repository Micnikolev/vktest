//
//  MessagesManager.swift
//  VKTest
//
//  Created by Michael Nikolaev on 25.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import UIKit

class MessagesManager {
    let userManager = UserManager()
    let communityManager = CommunityManager()
    let groupManager = GroupManager()
    
    func getMessagesHistory(accessToken: String, offset: Int, completion: @escaping (_ messages: [IMessage]) -> Void) {
        
        let historyRequest = VKRequest(string: "https://api.vk.com/method/messages.getDialogs?access_token=\(accessToken)&offset=\(offset)&unread=0&v=5.64")
        guard let urlRequest = historyRequest.urlRequest else { return }
        
        URLSession.shared.dataTask(with: urlRequest, completionHandler: {
            data, response, error in
            if let error = error {
                print(error.localizedDescription)
                completion([])
                return
            }
            guard let serializedData = try! JSONSerialization.jsonObject(with: data!, options: [.allowFragments]) as? [String:Any] else {
                completion([])
                return
            }
            
            guard let dataArray = serializedData["response"] as? [String: Any],
                let items = dataArray["items"] as? Array<NSDictionary> else {
                    completion([])
                    return
            }
            
            if items.count == 0 {
                completion([])
            }
            
            var messages = [IMessage]()
            for item in items {
                var typeOfMessage = MessageType.UserMessage
                
                if let message = item["message"] as? [String: Any] {
                    let date = message["date"] as? Int ?? 0
                    var uid = message["user_id"] as? Int ?? 0
                    let body = message["body"] as? String ?? ""
                    let unreadMessagesCount = item["unread"] as? Int ?? 0
                    
                    if let chatId = message["chat_id"] as? Int {
                        typeOfMessage = MessageType.GroupMessage
                        uid = chatId
                    } else if uid < 0 {
                        typeOfMessage = MessageType.CommunityMessage
                    }
                    
                    var isOut = false
                    if let out = message["out"] as? Int {
                        if out == 1 {
                            isOut = true
                        }
                    }
                    
                    var readState = MessageState.Read
                    if let read_state = message["read_state"] as? Int {
                        if read_state == 0 {
                            readState = .Unread
                        }
                    }
                    
                    var attachment: String?
                    if let attachments = message["attachments"] as? Array<AnyObject> {
                        if attachments.count > 1 {
                            attachment = "\(attachments.count) attachments"
                        } else {
                            if let attachName = attachments[0]["type"] as? String {
                                // In real usecase it should be localizable
                                if attachName == "link" {
                                    attachment = "Ссылка"
                                } else if attachName == "photo" {
                                    attachment = "Фотография"
                                } else if attachName == "video" {
                                    attachment = "Видеозапись"
                                } else if attachName == "doc" {
                                    attachment = "Документ"
                                } else if attachName == "market" {
                                    attachment = "Товар"
                                } else if attachName == "wall" {
                                    attachment = "Запись"
                                } else if attachName == "share" {
                                    attachment = "Ссылка"
                                } else if attachName == "sticker" {
                                    attachment = "Стикер"
                                } else if attachName == "gift" {
                                    attachment = "Подарок"
                                } else {
                                    attachment = "Вложение"
                                }
                            }
                        }
                    }
                    
                    if (message["fwd_messages"] != nil) {
                        attachment = "Сообщение"
                    }
                    
                    switch typeOfMessage {
                    case .UserMessage:
                        self.userManager.getUser(uid: uid, completion: {
                            user in
                            messages.append(UserMessage(text: body, lastMessageTime: Date(timeIntervalSince1970: TimeInterval(date)), attachment: attachment, unreadMessagesCount: unreadMessagesCount, lastMessageUser: nil, user: user, isOut: isOut, messageState: readState, isOnline: false))
                            if messages.count == items.count {
                                completion(messages)
                            }
                        })
                    case .CommunityMessage:
                        self.communityManager.getCommunity(uid: uid, accessToken: accessToken, completion: {
                            community in
                            
                            let communityMessage = CommunityMessage(text: body, lastMessageTime: Date(timeIntervalSince1970: TimeInterval(date)), attachment: attachment, unreadMessagesCount: unreadMessagesCount, community: community, lastMessageUser: nil, isOut: isOut, messageState: readState)
                            messages.append(communityMessage)
                            if messages.count == items.count {
                                completion(messages)
                            }
                        })
                        
                    case .GroupMessage:
                        guard let title = message["title"] as? String else { return }
                        if let lastUserId = message["user_id"] as? Int {
                            self.userManager.getUser(uid: lastUserId, completion: {
                                user in
                                self.groupManager.getGroupMessage(uid: uid, title: title, accessToken: accessToken, completion: {
                                    chat in
                                    let groupMessage = GroupMessage(title: title, text: body, lastMessageTime: Date(timeIntervalSince1970: TimeInterval(date)), attachment: attachment, unreadMessagesCount: unreadMessagesCount, group: chat, lastMessageUser: user, isOut: isOut, messageState: readState)
                                    messages.append(groupMessage)
                                    if messages.count == items.count {
                                        completion(messages)
                                    }
                                })
                            })
                        } else {
                            self.groupManager.getGroupMessage(uid: uid, title: title, accessToken: accessToken, completion: {
                                chat in
                                let groupMessage = GroupMessage(title: title, text: body, lastMessageTime: Date(timeIntervalSince1970: TimeInterval(date)), attachment: attachment, unreadMessagesCount: unreadMessagesCount, group: chat, lastMessageUser: nil, isOut: isOut, messageState: readState)
                                messages.append(groupMessage)
                                if messages.count == items.count {
                                    completion(messages)
                                }
                            })
                        }
                        
                    }
                    
                }
            }
            
        }).resume()
    }
}
