//
//  UserManager.swift
//  VKTest
//
//  Created by Michael Nikolaev on 24.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import UIKit
import VK_ios_sdk

class UserManager {
    
    var imageManager = ImageManager()
    
    func getUser(uid: Int, completion: @escaping (_ user: User)->Void) {
        
        let historyRequest = VKRequest(string: "https://api.vk.com/method/users.get?user_ids=\(uid)&fields=online,photo_100,verified")
        guard let urlRequest = historyRequest.urlRequest else { return }
        
        URLSession.shared.dataTask(with: urlRequest, completionHandler: {
            data, response, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let serializedData = try! JSONSerialization.jsonObject(with: data!, options: [.allowFragments]) as? [String:Any] else { return }
            
            guard let responseArray = serializedData["response"] as? Array<AnyObject> else { return }
            if let message = responseArray[0] as? [String: Any] {
                guard let firstName = message["first_name"] as? String,
                    let lastName = message["last_name"] as? String,
                    let photo = message["photo_100"] as? String else {
                        return
                }
                let uid = message["uid"] as? Int ?? 0
                let online = message["online"] as? Int ?? 0
                let verified = message["verified"] as? Int ?? 0
                
                var isVerified = false
                if verified == 1 {
                    isVerified = true
                }
                
                var onlineStatus = OnlineStatus.offline
                
                if online == 1 {
                    onlineStatus = .online
                    if message["online_mobile"] != nil {
                        onlineStatus = .onlineMobile
                    }
                }
                
                self.imageManager.downloadImage(url: URL(string: photo)!, completion: {
                    image in
                    let user = User(uid: uid, fullName: "\(firstName) \(lastName)", photo: image, verified: isVerified, muted: false, onlineStatus: onlineStatus)
                    completion(user)
                })
            }
        }).resume()
    }
}
