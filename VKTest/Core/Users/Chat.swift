//
//  Chat.swift
//  VKTest
//
//  Created by Michael Nikolaev on 24.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import UIKit

struct Chat: IUser {
    var uid: Int
    var fullName: String
    var photo: UIImage?
    var verified: Bool
    
    var muted: Bool
    var previewPhotos: [UIImage]?
}
