//
//  IUser.swift
//  VKTest
//
//  Created by Michael Nikolaev on 23.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import UIKit

protocol IUser {
    var uid: Int { get set }
    var fullName: String { get set }
    var photo: UIImage? { get set }
}
