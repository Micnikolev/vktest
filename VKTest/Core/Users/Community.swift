//
//  Community.swift
//  VKTest
//
//  Created by Michael Nikolaev on 24.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import UIKit

struct Community: IUser {
    var uid: Int
    var fullName: String
    var photo: UIImage?
    var verified: Bool
}
