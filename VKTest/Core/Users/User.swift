//
//  User.swift
//  VKTest
//
//  Created by Michael Nikolaev on 23.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import UIKit

enum OnlineStatus {
    case offline, online, onlineMobile
}

struct User: IUser {
    var uid: Int
    var fullName: String
    var photo: UIImage?
    var verified: Bool
    
    var muted: Bool
    var onlineStatus: OnlineStatus
}
