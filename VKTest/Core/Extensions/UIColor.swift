//
//  UIColor.swift
//  VKTest
//
//  Created by Michael Nikolaev on 23.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import UIKit

extension UIColor {
    static func mainBlueColor(alpha: CGFloat = 1.0) -> UIColor {
        return UIColor(red: 84/255, green: 129/255, blue: 187/255, alpha: alpha)
    }
}
