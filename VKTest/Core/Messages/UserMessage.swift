//
//  NormalMessage.swift
//  VKTest
//
//  Created by Michael Nikolaev on 23.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import UIKit

struct UserMessage: IMessage {
    var text: String
    var lastMessageTime: Date
    var attachment: String?
    var unreadMessagesCount: Int
    var lastMessageUser: User?
    var user: User
    var isOut: Bool
    var messageState: MessageState
    
    var isOnline: Bool
}
