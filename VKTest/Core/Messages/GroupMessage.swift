//
//  GroupMessage.swift
//  VKTest
//
//  Created by Michael Nikolaev on 23.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import UIKit

struct GroupMessage: IMessage {
    var title: String
    var text: String
    var lastMessageTime: Date
    var attachment: String?
    var unreadMessagesCount: Int
    var group: Chat
    var lastMessageUser: User?
    var isOut: Bool
    var messageState: MessageState
}
