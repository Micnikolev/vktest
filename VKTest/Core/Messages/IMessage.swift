//
//  Message.swift
//  VKTest
//
//  Created by Michael Nikolaev on 23.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import UIKit

enum MessageType {
    case UserMessage, CommunityMessage, GroupMessage
}

enum TypeOfMessage {
    case Out, In, None
}

enum MessageState {
    case Read, Unread
}

protocol IMessage {
    var text: String { get set }
    var lastMessageTime: Date { get set }
    var attachment: String? { get set }
    var unreadMessagesCount: Int { get set }
    var lastMessageUser: User? { get set }
    var isOut: Bool { get set }
    var messageState: MessageState { get set }
}
