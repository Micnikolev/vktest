//
//  IVKRequest.swift
//  VKTest
//
//  Created by Michael Nikolaev on 25.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import Foundation

protocol IVKRequest {
    var urlRequest: URLRequest? { get }
}
