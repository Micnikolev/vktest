//
//  VKRequest.swift
//  VKTest
//
//  Created by Michael Nikolaev on 25.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import Foundation

class VKRequest: IVKRequest {
    var urlRequest: URLRequest?
    
    init(string: String) {
        guard let url = URL(string: string) else {
            urlRequest = nil
            return
        }
        
        urlRequest = URLRequest(url: url)
    }
}
