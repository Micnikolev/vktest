//
//  IWKViewController.swift
//  VKTest
//
//  Created by Michael Nikolaev on 22.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import Foundation

protocol IVKViewController {
    func setUpUI()
}
