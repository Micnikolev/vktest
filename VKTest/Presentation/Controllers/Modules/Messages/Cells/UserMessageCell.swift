//
//  MessageTableViewCell.swift
//  VKTest
//
//  Created by Michael Nikolaev on 23.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import UIKit

class UserMessageCell: UITableViewCell, IVKMessageTableViewCell {
    
    var messageTitleLabel: UILabel!
    var messageTextLabel: UILabel!
    var placehodlerForUserImage: UIImageView!
    var innerTextImageView: UIImageView!
    var timeLabel: UILabel!
    var attachmentLabel: UILabel!
    var attachmentImage: UIImage!
    var onlineImageView: UIImageView!
    
    var unreadMessagesCircleView: UIView!
    var unreadMessagesLabel: UILabel!
    
    var verifiedImageView: UIImageView!
    var mutedImageView: UIImageView!
    
    var innerUserTextImageView: UIImageView!
    
    var typeOfMessage: TypeOfMessage!
    
    var userPhotos: [UIImage]! {
        didSet {
            placehodlerForUserImage.subviews.forEach { $0.removeFromSuperview() }
            placehodlerForUserImage.backgroundColor = .white
            switch userPhotos.count {
            case 0:
                placehodlerForUserImage.backgroundColor = .lightGray
            case 1:
               addUserPhotoImageView(image: userPhotos[0], customFrame: CGRect(x: 0, y: 0, width: placehodlerForUserImage.frame.width, height: placehodlerForUserImage.frame.height))
            case 2:
               addUserPhotoImageView(image: userPhotos[0], customFrame: CGRect(x: -1, y: 0, width: placehodlerForUserImage.frame.width/2, height: placehodlerForUserImage.frame.height))
                addUserPhotoImageView(image: userPhotos[1], customFrame: CGRect(x: placehodlerForUserImage.frame.width/2+1, y: 0, width: placehodlerForUserImage.frame.width, height: placehodlerForUserImage.frame.height))
            case 3:
                addUserPhotoImageView(image: userPhotos[0], customFrame: CGRect(x: -1, y: 0, width: placehodlerForUserImage.frame.width/2, height: placehodlerForUserImage.frame.height))
                addUserPhotoImageView(image: userPhotos[1], customFrame: CGRect(x: placehodlerForUserImage.frame.width/2+1, y: -1, width: placehodlerForUserImage.frame.width/2, height: placehodlerForUserImage.frame.height/2))
                addUserPhotoImageView(image: userPhotos[2], customFrame: CGRect(x: placehodlerForUserImage.frame.width/2+1, y: placehodlerForUserImage.frame.height/2+1, width: placehodlerForUserImage.frame.width/2, height: placehodlerForUserImage.frame.height/2))
            case 4:
                addUserPhotoImageView(image: userPhotos[0], customFrame: CGRect(x: -1, y: -1, width: placehodlerForUserImage.frame.width/2, height: placehodlerForUserImage.frame.height/2))
                addUserPhotoImageView(image: userPhotos[1], customFrame: CGRect(x: placehodlerForUserImage.frame.width/2+1, y: -1, width: placehodlerForUserImage.frame.width/2, height: placehodlerForUserImage.frame.height/2))
                addUserPhotoImageView(image: userPhotos[2], customFrame: CGRect(x: -1, y: placehodlerForUserImage.frame.height/2+1, width: placehodlerForUserImage.frame.width/2, height: placehodlerForUserImage.frame.height/2))
                addUserPhotoImageView(image: userPhotos[3], customFrame: CGRect(x: placehodlerForUserImage.frame.width/2+1, y: placehodlerForUserImage.frame.height/2+1, width: placehodlerForUserImage.frame.width/2, height: placehodlerForUserImage.frame.height/2))
            default:
                break
            }
        }
    }
    
    var onlineStatus: OnlineStatus! {
        didSet {
            switch onlineStatus {
            case .offline:
                onlineImageView.removeFromSuperview()
            case .online:
                onlineImageView.image = #imageLiteral(resourceName: "online_border_20")
                self.contentView.addSubview(onlineImageView)
            case .onlineMobile:
                onlineImageView.image = #imageLiteral(resourceName: "online_mobile_border_20")
                self.contentView.addSubview(onlineImageView)
            case .none:
                break
            case .some(_):
                break
            }
        }
    }
    
    var unreadMessagesCount: Int! {
        didSet {
            unreadMessagesCircleView.removeFromSuperview()
            if unreadMessagesCount != 0 {
                createUnreadMessagesView(number: unreadMessagesCount, type: typeOfMessage)
            }
        }
    }
    
    var verified: Bool! = false {
        didSet {
            if verified {
                messageTitleLabel.sizeToFit()
                var messageTitleLabelFrame = messageTitleLabel.frame
                if messageTitleLabel.frame.width > self.frame.width-140-12 {
                    messageTitleLabelFrame = CGRect(x: messageTitleLabel.frame.origin.x, y: messageTitleLabel.frame.origin.y+8, width: self.frame.width-140-12, height: messageTitleLabel.frame.height)
                }
                messageTitleLabelFrame = CGRect(x: messageTitleLabel.frame.origin.x, y: messageTitleLabel.frame.origin.y+8, width: messageTitleLabel.frame.width, height: messageTitleLabel.frame.height)
                messageTitleLabel.frame = messageTitleLabelFrame
                verifiedImageView.frame = CGRect(x: messageTitleLabel.frame.origin.x+messageTitleLabel.frame.width+5, y: messageTitleLabel.frame.origin.y+5, width: 16, height: 16)
                self.contentView.addSubview(verifiedImageView)
            } else {
                messageTitleLabel.frame = CGRect(x: placehodlerForUserImage.frame.origin.y + placehodlerForUserImage.frame.width + 12, y: 0, width: self.frame.width-140-12, height: 44)
                verifiedImageView.removeFromSuperview()
            }
        }
    }
    
    var muted: Bool! {
        didSet {
            if muted {
                messageTitleLabel.sizeToFit()
                var messageTitleLabelWidthDelta:CGFloat = 0
                if verified {
                    messageTitleLabelWidthDelta = 40
                }
                
                if messageTitleLabel.frame.width > self.frame.width-140-12 {
                    messageTitleLabel.frame = CGRect(x: messageTitleLabel.frame.origin.x, y: messageTitleLabel.frame.origin.y+8, width: self.frame.width-140-12-messageTitleLabelWidthDelta, height: messageTitleLabel.frame.height)
                }
                messageTitleLabel.frame = CGRect(x: messageTitleLabel.frame.origin.x, y: messageTitleLabel.frame.origin.y+8, width: messageTitleLabel.frame.width-messageTitleLabelWidthDelta, height: messageTitleLabel.frame.height)
                mutedImageView.frame = CGRect(x: messageTitleLabel.frame.origin.x+messageTitleLabel.frame.width+5, y: messageTitleLabel.frame.origin.y+2, width: 16, height: 16)
                self.contentView.addSubview(mutedImageView)
            } else {
               messageTitleLabel.frame = CGRect(x: placehodlerForUserImage.frame.origin.y + placehodlerForUserImage.frame.width + 12, y: 0, width: self.frame.width-140-12, height: 44)
                mutedImageView.removeFromSuperview()
            }
        }
    }
    
    var lastMessageUserImage: UIImage? {
        didSet {
            if let lastMessageUserImage = lastMessageUserImage {
                innerUserTextImageView.image = lastMessageUserImage
                self.contentView.addSubview(innerUserTextImageView)
                
                self.messageTextLabel.frame = CGRect(x: innerUserTextImageView.frame.origin.x+innerUserTextImageView.frame.width+8, y: messageTextLabel.frame.origin.y, width: self.contentView.frame.width-messageTitleLabel.frame.origin.x-timeLabel.frame.width-20-innerUserTextImageView.frame.width-8, height: messageTextLabel.frame.height)
                self.messageTextLabel.numberOfLines = 1
            } else {
                innerUserTextImageView.removeFromSuperview()
                self.messageTextLabel.frame = CGRect(x: messageTitleLabel.frame.origin.x, y: messageTextLabel.frame.origin.y, width: self.contentView.frame.width-messageTitleLabel.frame.origin.x-timeLabel.frame.width-8, height: 44)
                self.messageTextLabel.numberOfLines = 2
            }
        }
    }
    
    var messageState: MessageState! {
        didSet {
            if typeOfMessage == TypeOfMessage.Out {
                unreadMessagesCircleView.removeFromSuperview()
                if messageState == .Unread {
                    createUnreadMessagesView(number: 1, type: .Out)
                }
            }

        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setUpUI()
    }
    
    func addUserPhotoImageView(image: UIImage, customFrame: CGRect) {
        let customImageView = UIImageView(frame: customFrame)
        customImageView.contentMode = .scaleAspectFill
        customImageView.image = image
        customImageView.backgroundColor = UIColor.lightGray
        customImageView.clipsToBounds = true
        
        self.placehodlerForUserImage.addSubview(customImageView)
    }
    
    func createPlaceholderForPhotos() {
        placehodlerForUserImage = UIImageView(frame: CGRect(x: 8, y: 8, width: 60, height: 60))
        placehodlerForUserImage.backgroundColor = UIColor.lightGray
        placehodlerForUserImage.layer.cornerRadius = placehodlerForUserImage.frame.width/2
        placehodlerForUserImage.clipsToBounds = true
    }
    
    func createGiftAttachment() {
        if lastMessageUserImage == nil {
            self.innerTextImageView.frame = CGRect(x: messageTitleLabel.frame.origin.x, y: messageTextLabel.frame.origin.y+12, width: 20, height: 20)
            self.messageTextLabel.frame = CGRect(x: innerTextImageView.frame.origin.x+25, y: messageTextLabel.frame.origin.y, width: messageTextLabel.frame.width-20, height: messageTextLabel.frame.height)
        } else {
            self.innerTextImageView.frame = CGRect(x: innerUserTextImageView.frame.origin.x+innerUserTextImageView.frame.width+6, y: innerTextImageView.frame.origin.y, width: innerTextImageView.frame.width, height: innerTextImageView.frame.height)
            self.messageTextLabel.frame = CGRect(x: innerTextImageView.frame.origin.x+innerTextImageView.frame.width+6, y: messageTextLabel.frame.origin.y, width: messageTextLabel.frame.width-20, height: messageTextLabel.frame.height)
        }
        innerTextImageView.image = #imageLiteral(resourceName: "gift_16")
        messageTextLabel.text = "Подарок"
        self.contentView.addSubview(innerTextImageView)
    }
    
    func createUnreadMessagesView(number: Int, type: TypeOfMessage) {
        if type == .In {
            unreadMessagesCircleView = UIView(frame: CGRect(x: self.timeLabel.frame.origin.x+8, y: self.timeLabel.frame.origin.y+40, width: 22, height: 22))
            unreadMessagesLabel = UILabel(frame: CGRect(x: 0, y: 0, width: unreadMessagesCircleView.frame.width, height: unreadMessagesCircleView.frame.height))
            unreadMessagesCircleView.backgroundColor = UIColor.mainBlueColor()
            
            unreadMessagesLabel.text = String(number)
            unreadMessagesLabel.textAlignment = .center
            unreadMessagesLabel.font = UIFont.systemFont(ofSize: 13, weight: .regular)
            unreadMessagesLabel.textColor = UIColor.white
            
            unreadMessagesCircleView.addSubview(unreadMessagesLabel)
        } else {
            unreadMessagesCircleView = UIView(frame: CGRect(x: self.timeLabel.frame.origin.x+12, y: self.timeLabel.frame.origin.y+46, width: 8, height: 8))
            unreadMessagesCircleView.backgroundColor = UIColor.mainBlueColor(alpha: 0.5)
        }
        unreadMessagesCircleView.layer.cornerRadius = unreadMessagesCircleView.frame.width/2
        self.contentView.addSubview(unreadMessagesCircleView)
    }
    
    func setUpUI(){
        createPlaceholderForPhotos()
        
        messageTitleLabel = UILabel(frame: CGRect(x: placehodlerForUserImage.frame.origin.y + placehodlerForUserImage.frame.width + 12, y: 0, width: self.frame.width-140-12, height: 44))
        messageTitleLabel.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        messageTitleLabel.numberOfLines = 1
        
        timeLabel = UILabel(frame: CGRect(x: UIScreen.main.bounds.width-44, y: 0, width: 44, height: 44))
        timeLabel.textAlignment = .left
        timeLabel.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        timeLabel.textColor = UIColor.darkGray
        
        messageTextLabel = UILabel(frame: CGRect(x: messageTitleLabel.frame.origin.x, y: messageTitleLabel.frame.origin.y+messageTitleLabel.frame.height-16, width:  self.contentView.frame.width-messageTitleLabel.frame.origin.x-timeLabel.frame.width-8, height: 44))
        messageTextLabel.numberOfLines = 2
        messageTextLabel.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        messageTextLabel.textColor = UIColor.darkGray
        
        innerUserTextImageView = UIImageView(frame: CGRect(x: messageTitleLabel.frame.origin.x, y: messageTextLabel.frame.origin.y+12, width: 25, height: 25))
        innerUserTextImageView.layer.cornerRadius = innerUserTextImageView.frame.width/2
        innerUserTextImageView.clipsToBounds = true
        
        innerTextImageView = UIImageView(frame: CGRect(x: messageTitleLabel.frame.origin.x, y: messageTextLabel.frame.origin.y+12, width: 20, height: 20))
        innerTextImageView.layer.cornerRadius = innerTextImageView.frame.width/2
        innerTextImageView.clipsToBounds = true
        
        onlineImageView = UIImageView(frame: CGRect(x: placehodlerForUserImage.frame.width + placehodlerForUserImage.frame.origin.x-18, y: placehodlerForUserImage.frame.height+placehodlerForUserImage.frame.origin.y-18, width: 20, height: 20))
        onlineImageView.contentMode = .scaleAspectFit
        onlineImageView.clipsToBounds = true
        
        verifiedImageView = UIImageView(frame: CGRect(x: messageTitleLabel.frame.origin.x+messageTitleLabel.frame.width+5, y: messageTitleLabel.frame.origin.y+5, width: 16, height: 16))
        verifiedImageView.image = #imageLiteral(resourceName: "verified_16")
        verifiedImageView.contentMode = .scaleAspectFit
        
        mutedImageView = UIImageView(frame: CGRect(x: messageTitleLabel.frame.origin.x+messageTitleLabel.intrinsicContentSize.width, y: messageTitleLabel.frame.origin.y, width: 16, height: 16))
        mutedImageView.image = #imageLiteral(resourceName: "muted_16")
        mutedImageView.contentMode = .scaleAspectFit
        
        unreadMessagesCircleView = UIView(frame: CGRect(x: self.timeLabel.frame.origin.x+8, y: self.timeLabel.frame.origin.y+40, width: 22, height: 22))
        
        self.contentView.addSubview(placehodlerForUserImage)
        self.contentView.addSubview(timeLabel)
        self.contentView.addSubview(messageTitleLabel)
        self.contentView.addSubview(messageTextLabel)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init(coder: aDecoder)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

