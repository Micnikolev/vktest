//
//  MessagesViewController.swift
//  VKTest
//
//  Created by Michael Nikolaev on 22.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import UIKit
import VK_ios_sdk

class MessagesViewController: UITableViewController, IVKViewController {
    
    var activityIndicator: UIActivityIndicatorView!
    
    fileprivate let sizeOfRow:CGFloat = 76
    
    private var messages = [IMessage]()
    private var offsetForDialogs = 0
    private var loadMoreStatus = false
    
    var currentUser: User!
    
    var accessToken: VKAccessToken!
    
    let networkService = NetworkService()
    let userManager = UserManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = UIColor.mainBlueColor()
        
        setUpUI()
        loadMessages()
        registerCells()
        setUpRefresh()
    }
    
    func setUpRefresh() {
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(loadMessages), for: .valueChanged)
    }
    
    func registerCells() {
        tableView.register(UserMessageCell.self, forCellReuseIdentifier: "UserMessage")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func loadMessages(moreMessages: Bool = false) {
        if !moreMessages {
            offsetForDialogs = 0
        }
        
        userManager.getUser(uid: Int(accessToken.userId)!, completion: {
            [unowned self] user in
            self.currentUser = user
            self.updateTableViewWithMessages(accessToken: self.accessToken.accessToken, moreMessages: moreMessages)
        })
    }
    
    func updateTableViewWithMessages(accessToken: String, moreMessages: Bool) {
        networkService.getMessages(accessToken: accessToken, offset: offsetForDialogs, completion: { [unowned self]
            messages in
            let sortedNewMessages = messages.sorted(by: {$0.lastMessageTime > $1.lastMessageTime })
            if moreMessages {
                self.messages += sortedNewMessages
            } else {
                self.messages = sortedNewMessages
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
                if messages.count != 0 {
                    self.loadMoreStatus = false
                }
                self.tableView.tableFooterView?.isHidden = true
                self.refreshControl?.endRefreshing()
            }
        })
    }
    
    func setUpUI() {
        self.title = "Сообщения"
        
        tableView.allowsSelection = false
        tableView.tableFooterView?.isHidden = true
        
        let footer = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 60))
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: self.view.frame.width/2-22, y: 10, width: 44, height: 44))
        activityIndicator.activityIndicatorViewStyle = .gray
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        footer.addSubview(activityIndicator)
        tableView.tableFooterView = footer
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        
        if let userCell = tableView.dequeueReusableCell(withIdentifier: "UserMessage", for: indexPath) as? UserMessageCell {
            cell = userCell
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let userCell = cell as? UserMessageCell else { return }
        
        let message = messages[indexPath.row]

        userCell.timeLabel.text = message.lastMessageTime.convertToString(format: "HH:MM")
        userCell.innerTextImageView.removeFromSuperview()
        
        if message.isOut {
            userCell.typeOfMessage = .Out
            userCell.lastMessageUserImage = currentUser.photo
        } else {
            userCell.typeOfMessage = .In
            if let lastMessageUser = message.lastMessageUser {
                userCell.lastMessageUserImage = lastMessageUser.photo
            } else {
                userCell.lastMessageUserImage = nil
            }
        }
        
        if let attachment = message.attachment {
            if message.attachment == "Подарок" {
                userCell.createGiftAttachment()
            } else {
                userCell.messageTextLabel.text = attachment.localizedCapitalized
                userCell.messageTextLabel.textColor = UIColor.mainBlueColor()
            }
        } else {
            userCell.messageTextLabel.text = message.text
            userCell.messageTextLabel.textColor = UIColor.darkGray
        }
        
        userCell.unreadMessagesCount = message.unreadMessagesCount
        
        userCell.messageState = message.messageState
        
        if let userMessage = message as? UserMessage {
            userCell.messageTitleLabel.text = userMessage.user.fullName
            userCell.userPhotos = [userMessage.user.photo!]
            userCell.onlineStatus = userMessage.user.onlineStatus
            
            userCell.verified = userMessage.user.verified
            userCell.muted = userMessage.user.muted
        } else if let communityMessage = message as? CommunityMessage {
            userCell.messageTitleLabel.text = communityMessage.community.fullName
            userCell.messageTextLabel.text = communityMessage.text
            if let photo = communityMessage.community.photo {
                userCell.userPhotos = [photo]
            } else {
                userCell.userPhotos = []
            }
            userCell.unreadMessagesCount = communityMessage.unreadMessagesCount
            userCell.onlineStatus = OnlineStatus.offline
            userCell.verified = communityMessage.community.verified
            userCell.muted = false
        } else if let groupMessage = message as? GroupMessage {
            userCell.messageTitleLabel.text = groupMessage.title
            if let photos = groupMessage.group.previewPhotos {
                userCell.userPhotos = photos
            } else if let photo = groupMessage.group.photo {
                    userCell.userPhotos = [photo]
            } else {
                userCell.userPhotos = []
            }
            userCell.unreadMessagesCount = groupMessage.unreadMessagesCount
            userCell.onlineStatus = OnlineStatus.offline
            userCell.verified = false
            userCell.muted = groupMessage.group.muted
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return sizeOfRow
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        let deltaOffset = maximumOffset - currentOffset
        
        if deltaOffset <= 0 {
            loadMore()
        }
    }
    
    func loadMore() {
        if ( !loadMoreStatus ) {
            self.loadMoreStatus = true
            loadMoreBegin()
        }
    }
    
    func loadMoreBegin() {
        tableView.tableFooterView?.isHidden = false
        DispatchQueue.global(qos: .userInteractive).async {
            self.offsetForDialogs += 20
            DispatchQueue.main.async {
                self.loadMessages(moreMessages: true)
            }
        }
    }
}
