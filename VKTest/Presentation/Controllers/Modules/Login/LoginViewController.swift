//
//  ViewController.swift
//  VKTest
//
//  Created by Michael Nikolaev on 22.12.2017.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import UIKit
import VK_ios_sdk

class LoginViewController: UIViewController, IVKViewController {
    
    var titleLabel: UILabel!
    var authButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpVKApp()
        setUpUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setUpVKApp() {
        let vkInstance = VKSdk.initialize(withAppId: "5044697")
        vkInstance?.uiDelegate = self
        vkInstance?.register(self)
    }
    
    func setUpUI() {
        self.title = "Вход"
        
        authButton = UIButton(type: .system)
        authButton.setTitleColor(UIColor.mainBlueColor(), for: .normal)
        authButton.frame = CGRect(x: 25, y: self.view.frame.height/2-44, width: self.view.frame.width-25-25, height: 44)
        authButton.setTitle("Войти в VK", for: .normal)
        authButton.addTarget(self, action: #selector(authToVK), for: UIControlEvents.touchUpInside)
        
        titleLabel = UILabel(frame: CGRect(x: 25, y: self.authButton.frame.origin.y-25-44, width: self.view.frame.width-25-25, height: 44))
        titleLabel.text = "Добро пожаловать в ВК Тест!"
        titleLabel.textAlignment = .center
        
        self.view.addSubview(authButton)
        self.view.addSubview(titleLabel)
    }
    
    @objc func authToVK(sender: AnyObject) {
        VKSdk.wakeUpSession([VK_API_LONG], complete: {
            state, error in
            if state == .initialized {
                VKSdk.authorize([VK_PER_MESSAGES])
            }
        })
    }
    
    func openMessages(accessToken: VKAccessToken) {
        let messagesViewController = MessagesViewController()
        messagesViewController.accessToken = accessToken
        
        let navVC = UINavigationController(rootViewController: messagesViewController)
        navVC.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navVC.navigationBar.barTintColor = UIColor.mainBlueColor()
        navVC.navigationBar.isTranslucent = false
        navVC.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        self.present(navVC, animated: false, completion: nil)
    }
}

extension LoginViewController: VKSdkDelegate, VKSdkUIDelegate {
    func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
        if result.state == .error {
            self.dismiss(animated: true, completion: nil)
        } else {
            guard let accessToken = VKSdk.accessToken() else { return }
            self.dismiss(animated: true, completion: nil)
            openMessages(accessToken: accessToken)
        }
    }
    
    func vkSdkUserAuthorizationFailed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func vkSdkShouldPresent(_ controller: UIViewController!) {
        self.present(controller, animated: true, completion: nil)
    }
    
    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        let vc = VKCaptchaViewController.captchaControllerWithError(captchaError)
        vc?.present(in: self.navigationController?.topViewController)
    }
}

